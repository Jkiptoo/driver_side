class Address {
  String? placeFormattedAddress;
  String? latitude;
  String? longitude;

  String? placeName;
  String? placeId;

  Address({
    this.placeFormattedAddress,
    this.placeName,
    this.placeId,
    this.latitude,
    this.longitude,
  });
}
