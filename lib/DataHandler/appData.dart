import 'package:flutter/cupertino.dart';
import 'package:e_mechanic/Model/address.dart';

class AppData with ChangeNotifier {
  Address? pickupLocation, dropOffLocation;

  void updatePickupLocationAddress(Address pickUpAddress) {
    pickupLocation = pickUpAddress;
    notifyListeners();
  }

  void updateDropOffLocationAddress(Address DropOffAddress) {
    dropOffLocation = DropOffAddress;
    notifyListeners();
  }
}
