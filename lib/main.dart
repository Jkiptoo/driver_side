import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:provider/provider.dart';
import 'package:e_mechanic/DataHandler/appData.dart';
import 'package:e_mechanic/screen/loginScreen.dart';
import 'package:e_mechanic/screen/main_screen.dart';
import 'package:e_mechanic/screen/registrationScreen.dart';
import 'package:e_mechanic/screen/welcomeScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    await Firebase.initializeApp();
  } catch (e) {
    print('Firebase initialization error: $e');
  }
  runApp(const MyApp());
}

DatabaseReference userRef = FirebaseDatabase.instance.ref().child('users');

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // Initialize Provider with changeNotifierProvider
    return ChangeNotifierProvider(
      create: (context) => AppData(),
      child: MaterialApp(
        title: 'Taxi Rider App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: "Signatra",
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: FirebaseAuth.instance.currentUser == null ?  WelcomeScreen() : const MainScreen(),
        routes: {
          '/register': (context) => RegistrationScreen(),
          '/login': (context) => LoginScreen(),
          '/welcome': (context) => const WelcomeScreen(),
          '/main': (context) => const MainScreen()
        },
      ),
    );
  }
}
