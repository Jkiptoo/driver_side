import 'dart:async';

import 'package:e_mechanic/Model/allUsers.dart';
import 'package:e_mechanic/Model/directionDetails.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:e_mechanic/Assistants/requestAssistant.dart';
import 'package:e_mechanic/Model/address.dart';
import 'package:e_mechanic/api/configMaps.dart';

import '../DataHandler/appData.dart';

class AssistantMethods {
  static Future<String> searchCoordinateAddress(
      Position position, context) async {
    String placeAddress = "";
    String st1, st2;

    String url =
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=$mapKey";
    var response = await RequestAssistant.getRequest(url);

    if (response != 'failed') {
      // placeAddress = response["results"[0]["formatted_address"];
      st1 = response["results"][0]["address_components"][1]["long_name"];
      st2 = response["results"][0]["address_components"][2]["long_name"];

      placeAddress = "$st1, $st2";

      Address userPickUpAddress = Address();
      userPickUpAddress.longitude = position.longitude.toString();
      userPickUpAddress.latitude = position.latitude.toString();
      userPickUpAddress.placeName = placeAddress;

      Provider.of<AppData>(context, listen: false)
          .updatePickupLocationAddress(userPickUpAddress);
    }
    return placeAddress;
  }

  static Future<DirectionDetails?> obtainPlaceDirectionDetails(
      LatLng initialPosition, LatLng finalPosition) async {
    String directionUrl =
        "https://maps.googleapis.com/maps/api/directions/json?origin= ${initialPosition.latitude},${initialPosition.longitude}&destination=${finalPosition.latitude},${finalPosition.longitude}&key=$mapKey";
    print("++++++++++++++++++++++++++");
    print("++++++++++++++++++++++++++");
    print("${finalPosition.longitude}, ${finalPosition.latitude}");
    print("++++++++++++++++++++++++++");
    print("++++++++++++++++++++++++++");
    print("${initialPosition.latitude},${finalPosition.latitude}");
    print("++++++++++++++++++++++++++");
    print("++++++++++++++++++++++++++");
    print("${initialPosition.longitude},${finalPosition.longitude}");

    var res = await RequestAssistant.getRequest(directionUrl);
    if (res == "failed") {
      return null; // Return null if the request fails
    }

    print(res);

    DirectionDetails directionDetails = DirectionDetails();

    if (res.containsKey('routes') &&
        res['routes'] is List &&
        res['routes'].isNotEmpty) {
      directionDetails.encodedPoints =
          res["routes"][0]["overview_polyline"]["points"];
      directionDetails.distanceText =
          res["routes"][0]["legs"][0]["distance"]["text"];
      directionDetails.distanceValue =
          res["routes"][0]["legs"][0]["distance"]["value"];
      directionDetails.durationText =
          res["routes"][0]["legs"][0]["duration"]["text"];
      directionDetails.durationValue =
          res["routes"][0]["legs"][0]["duration"]["value"];
    } else {
      // scenario when 'routes' array is empty or doesn't exist
      print('Routes are not available in the response.');
      return null; //null if routes are not found
    }

    return directionDetails;
  }

//   calculate tow rates using distance and duration
  static int calculateTowRates(DirectionDetails directionDetails) {
    if (directionDetails.durationValue != null && directionDetails.distanceValue != null) {
      double timeTraveledRate = (directionDetails.durationValue! / 60) * 0.20;
      double distanceTraveledRate = (directionDetails.distanceValue! / 1000) * 0.20;
      double totalRate = timeTraveledRate + distanceTraveledRate;

      // Convert USD to kes
      double total = totalRate * 151.53;

      return total.truncate();
    } else {
      // Handle the case when durationValue or distanceValue is null
      return 0; // Or another default value or handle the situation differently
    }
  }

  static void getCurrentOnlineUserInfo() {
    firebaseUser = FirebaseAuth.instance.currentUser;
    String userId = firebaseUser!.uid;
    DatabaseReference reference = FirebaseDatabase.instance.ref()
        .child('users')
        .child(userId);

    reference.onValue.listen((event) {
      if (event.snapshot.value != null) {
        firebaseUsers = Users.fromSnapshot(event.snapshot);
      }
    });


  }

  }
