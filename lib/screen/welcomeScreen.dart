import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';
import 'package:e_mechanic/screen/loginScreen.dart';


class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
      animationDuration: 3,
      pages: [

        PageViewModel(
          title: "E_Mechanic app".toUpperCase(),
          body: 'To the next level with you',
          image: const Image(image: AssetImage('images/driver.jpg'),),
          decoration: getPageDecoration(),
        ),
        // PageViewModel(
        //   title: "Modern farming",
        //   body: 'Get your records with a touch of a button',
        //   image: buildLottie('asset/animation_llmdzkss.json'),
        //   decoration: getPageDecoration(),
        // ),
      ],
      done: Text(
        'Explore',
        style: TextStyle(
            fontSize: 25, fontWeight: FontWeight.bold, color: Colors.grey[800]),
      ),
      onDone: () {
        Navigator.push(context, MaterialPageRoute(builder: (context)=> LoginScreen()));
      },
      next: const Icon(Icons.arrow_forward),
    );
  }


  PageDecoration getPageDecoration() {
    return PageDecoration(
        titleTextStyle:
        const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
        bodyTextStyle: const TextStyle(
          fontSize: 20,
        ),
        bodyPadding: const EdgeInsets.all(16).copyWith(bottom: 0),
        imagePadding: const EdgeInsets.all(24));
  }

  Widget buildLottie(String path) {
    return Center(
      child: Lottie.asset(
        path,
      ),
    );
  }

  Widget buildImage(String path) {
    return Center(
      child: Image.asset(
        path,
      ),
    );
  }
}
