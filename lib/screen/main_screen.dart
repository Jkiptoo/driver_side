import 'dart:async';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:e_mechanic/Model/directionDetails.dart';
import 'package:e_mechanic/Widget/progressDialog.dart';
import 'package:e_mechanic/api/configMaps.dart';
import 'package:e_mechanic/screen/searchScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:e_mechanic/DataHandler/appData.dart';
import 'package:e_mechanic/Widget/Divider.dart';
import '../Assistants/assistantMethods.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with TickerProviderStateMixin {
  /*======================Getting user current position==================================*/
  late Position currentPosition;
  var geolocator = Geolocator();
  double bottomPaddingOfMap = 0;

  Set<Marker> markersSet = {};
  Set<Circle> circlesSet = {};

  void locatePosition() async {
    LocationPermission permission;
    permission = await Geolocator.checkPermission();

    try {
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.deniedForever) {
          return Future.error("Location not available!");
        } else {
          throw Exception('Error');
        }
      }
    } catch (e) {
      print('Exception in Location: $e');
    }
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    currentPosition = position;
    LatLng latLngPosition = LatLng(position.latitude, position.longitude);
    //   instance where camera moves according to location
    CameraPosition cameraPosition =
        CameraPosition(target: latLngPosition, zoom: 14);
    newGoogleMapController
        .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    String address =
        await AssistantMethods.searchCoordinateAddress(position, context);
    print('This is your address' + address);
    print(
        '${Provider.of<AppData>(context, listen: false).pickupLocation?.placeName}');
  }

  // Using coordinates to create polyline points between the origin and the destination
  final Completer<GoogleMapController> _controllerGoogleMap =
      Completer<GoogleMapController>();

  late GoogleMapController newGoogleMapController;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  late DirectionDetails tripDirectionDetails = DirectionDetails();

  List<LatLng> plineCoordinates = [];
  Set<Polyline> polylineSet = {};

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(36.817223, -1.286389),
    zoom: 14.4746,
  );

  // switching in between the location stack and the request stack
  double towDetailsContainerHeight = 0;
  double setSearchContainerHeight = 300.0;
  double requestTowContainerHeight = 0;
  bool drawerOpen = true;

  resetApp() {
    setState(() {
      drawerOpen = true;
      setSearchContainerHeight = 300.0;
      towDetailsContainerHeight = 0;
      bottomPaddingOfMap = 230.0;
      requestTowContainerHeight = 0;

      polylineSet.clear();
      markersSet.clear();
      circlesSet.clear();
      plineCoordinates.clear();
    });
    locatePosition();
  }

  void displayRequestStack() async {
    await getPlaceDirection();
    setState(() {
      setSearchContainerHeight = 0;
      towDetailsContainerHeight = 230.0;
      bottomPaddingOfMap = 230.0;
      drawerOpen = false;
    });
    saveTowData();
  }
  void requestDisplayTowContainer()  {
    setState(() {
      requestTowContainerHeight = 250.0;
      towDetailsContainerHeight = 0;
      bottomPaddingOfMap = 230.0;
      drawerOpen = true;
    });
  }
  DatabaseReference? towRequestRef;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AssistantMethods.getCurrentOnlineUserInfo();
  }


  // save users previous data in firebase
  void saveTowData() {
    towRequestRef = FirebaseDatabase.instance.ref().child('Tow Request').push();

    var pickUp = Provider.of<AppData>(context, listen: false).pickupLocation;
    var dropOff = Provider.of<AppData>(context, listen: false).dropOffLocation;

    if (pickUp != null && dropOff != null) {
      Map<String, dynamic> pickUpLocation = {
        "latitude": pickUp.latitude.toString(),
        "longitude": pickUp.longitude.toString(),
      };
      Map<String, dynamic> dropOffLocMap = {
        "latitude": dropOff.latitude,
        "longitude": dropOff.longitude,
      };
      print('================================================================');
      print(dropOff.latitude);
      print(firebaseUsers?.name);
      print('================================================================');

      Map<String, dynamic> towInfoMap = {
        "mechanic_id": "waiting",
        "payment_method": "cash",
        "created_at": DateTime.now().toString(),
        "rider_name": firebaseUsers?.name ?? '',
        "rider_email": firebaseUsers?.email ?? '',
        "rider_phone": firebaseUsers?.phone ?? '',
        "pickup_address": pickUp.placeName ?? '',
        "dropoff_address": dropOff.placeName ?? '',
      };
      towRequestRef!.set(towInfoMap);
    } else {
      // Handle the case where pickUp or dropOff is null
      print('Pickup or dropOff is null');
    }
  }

  // Cancelling tow request
  void cancelTowRequest() {
    towRequestRef!.remove();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: const Text('Main Screen'),
      ),
      drawer: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width * 0.57,
        child: Drawer(
          child: ListView(
            children: [
              // drawer header
              SizedBox(
                height: 165,
                child: DrawerHeader(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SingleChildScrollView(
                    child: Row(
                      children: [
                        Image.asset(
                          'images/user_icon.png',
                          height: 65.0,
                          width: 65,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        const Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Profile Name',
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: "Brand-Bold",
                              ),
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Text('Visit Profile'),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              const DividerWidget(),
              const SizedBox(
                height: 12,
              ),
              //   drawer body
              const ListTile(
                leading: Icon(Icons.history),
                title: Text(
                  'History',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              const ListTile(
                leading: Icon(Icons.person),
                title: Text(
                  'Visit Profile',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              const ListTile(
                leading: Icon(Icons.info),
                title: Text(
                  'About',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              GestureDetector(
                onTap: (){
                  FirebaseAuth.instance.signOut().then((value) {
                    Navigator.pushNamedAndRemoveUntil(context, '/welcome', (route) => false);
                  });

                },
                child: const ListTile(
                  leading: Icon(Icons.info),
                  title: Text(
                    'Log Out',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Stack(
        children: [
          GoogleMap(
            padding: EdgeInsets.only(bottom: bottomPaddingOfMap),
            mapType: MapType.normal,
            myLocationButtonEnabled: true,
            initialCameraPosition: _kGooglePlex,
            myLocationEnabled: true,
            zoomGesturesEnabled: true,
            zoomControlsEnabled: true,
            polylines: polylineSet,
            markers: markersSet,
            circles: circlesSet,
            onMapCreated: (GoogleMapController controller) {
              _controllerGoogleMap.complete(controller);
              newGoogleMapController = controller;

              setState(() {
                bottomPaddingOfMap = 300;
              });
              locatePosition();
            },
          ),
          // hamburgerButton for drawer Widget
          Positioned(
              top: 38,
              left: 22,
              child: GestureDetector(
                onTap: () {
                  if (drawerOpen) {
                    scaffoldKey.currentState!.openDrawer();
                  } else {
                    resetApp();
                  }
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(22),
                      boxShadow: const [
                        BoxShadow(
                            color: Colors.white,
                            blurRadius: 6,
                            spreadRadius: 0.5,
                            offset: Offset(0.7, 0.7))
                      ]),
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 20,
                    child: Icon(
                      (drawerOpen) ? Icons.menu : Icons.close,
                      color: Colors.black,
                    ),
                  ),
                ),
              )),

          // Display the bottom stack to display users location
          Positioned(
              left: 0.0,
              right: 0.0,
              bottom: 0.0,
              child: AnimatedSize(
                curve: Curves.bounceIn,
                duration: new Duration(milliseconds: 160),
                child: Container(
                    height: setSearchContainerHeight,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(18),
                            topRight: Radius.circular(18)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black,
                              blurRadius: 16,
                              spreadRadius: 0.5,
                              offset: Offset(0.7, 0.7))
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 24, vertical: 18),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 5,
                          ),
                          const Text(
                            'Hey there',
                            style: TextStyle(fontSize: 12),
                          ),
                          const Text(
                            'Where to',
                            style: TextStyle(
                                fontSize: 20, fontFamily: "Brand-Bold"),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          GestureDetector(
                            onTap: () async {
                              var res = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const SearchScreen()));
                              if (res == "obtainDirection") {
                                displayRequestStack();
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Colors.black54,
                                        blurRadius: 6,
                                        spreadRadius: 0.5,
                                        offset: Offset(0.7, 0.7))
                                  ]),
                              child: const Padding(
                                padding: EdgeInsets.all(12),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.search,
                                      color: Colors.blueAccent,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text('Search Drop Off Location')
                                  ],
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          Row(
                            children: [
                              const Icon(
                                Icons.home,
                                color: Colors.grey,
                              ),
                              const SizedBox(
                                width: 12.0,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${Provider.of<AppData>(context).pickupLocation?.placeName}',
                                  ),
                                  const SizedBox(
                                    height: 24.0,
                                  ),
                                  Text(
                                    'Residential address',
                                    style: TextStyle(
                                        color: Colors.grey.shade700,
                                        fontSize: 12),
                                  )
                                ],
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const DividerWidget(),
                          const SizedBox(
                            height: 16,
                          ),
                          const Row(
                            children: [
                              Icon(
                                Icons.work,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                width: 12.0,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Add Work'),
                                  SizedBox(
                                    height: 24.0,
                                  ),
                                  Text(
                                    'Office address',
                                    style: TextStyle(
                                        color: Colors.black54, fontSize: 12),
                                  )
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    )),
              )),
          // Display tow trucks request options
          Positioned(
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: AnimatedSize(
                curve: Curves.bounceIn,
                duration: new Duration(milliseconds: 160),
                child: Container(
                  height: towDetailsContainerHeight,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(
                            16.0,
                          ),
                          topRight: Radius.circular(
                            16.0,
                          )),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 16,
                          spreadRadius: 0.5,
                          offset: Offset(0.7, 0.7),
                        )
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 17, horizontal: 10),
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.tealAccent.shade100,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              children: [
                                Image.asset(
                                  'images/taxi.png',
                                  height: 70,
                                  width: 80,
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        const Text(
                                          'Tow Truck',
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontFamily: "Brand-Bold"),
                                        ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Kes ${AssistantMethods.calculateTowRates(tripDirectionDetails)}',
                                          style: const TextStyle(
                                              fontSize: 18,
                                              fontFamily: "Brand-Bold",
                                              color: Colors.grey),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      tripDirectionDetails.distanceText ??
                                          'null',
                                      style: const TextStyle(
                                          fontSize: 18,
                                          fontFamily: "Brand-Bold",
                                          color: Colors.grey),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(height: 20),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            children: [
                              Icon(
                                FontAwesomeIcons.moneyCheckDollar,
                                size: 18,
                                color: Colors.black54,
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text('Cash'),
                              SizedBox(
                                width: 6,
                              ),
                              Icon(
                                Icons.keyboard_arrow_down,
                                color: Colors.black,
                                size: 16,
                              )
                            ],
                          ),
                        ),
                        const SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.blueAccent,
                            ),
                            onPressed: () {
                              requestDisplayTowContainer();
                            },
                            child: const Padding(
                              padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Request',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  Icon(
                                    FontAwesomeIcons.truck,
                                    color: Colors.white,
                                    size: 18,
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )),
          //   Request ride panel
          Positioned(
            bottom: 0.0,
            right: 0.0,
            left: 0.0,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16.0),
                      topRight: Radius.circular(16)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 0.5,
                        blurRadius: 16,
                        color: Colors.black54,
                        offset: Offset(0.7, 0.7)),
                  ]),
              height: requestTowContainerHeight,
              child: Padding(
                padding: EdgeInsets.all(30),
                child: Column(
                  children: [
                    SizedBox(
                      height: 12.0,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: AnimatedTextKit(
                        animatedTexts: [
                          buildColorizedText('Requesting Tow truck', 'Signatra'),
                          buildColorizedText('Please wait', 'Signatra'),
                          buildColorizedText('Finding a Mechanic', 'Signatra'),
                        ],
                        isRepeatingAnimation: true,
                        onTap: () {
                          print("Tap Event");
                        },
                      ),
                    ),
                    SizedBox(height: 22,),
                    GestureDetector(
                      onTap: (){
                        cancelTowRequest();
                        resetApp();
                      },
                      child: Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          color: Colors.white, borderRadius: BorderRadius.circular(26),
                          border: Border.all(width: 2.0, color: Colors.grey.shade300),
                        ),
                        child: Icon(Icons.close, size: 26,),
                      ),
                    ),
                    SizedBox(height: 10,),


                       Container(
                        width: double.infinity,
                        child: Text('Cancel Tow', textAlign: TextAlign.center, style: TextStyle(fontSize: 12),),
                      ),


                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> getPlaceDirection() async {
    var initialPosn =
        Provider.of<AppData>(context, listen: false).pickupLocation;
    var finalPosn =
        Provider.of<AppData>(context, listen: false).dropOffLocation;

    if (initialPosn != null && finalPosn != null) {
      var pickUpLatLng = LatLng(
        double.parse(initialPosn.latitude ?? '0.0'),
        double.parse(initialPosn.longitude ?? '0.0'),
      );
      var dropOffLatLng = LatLng(
        double.parse(finalPosn.latitude ?? '0.0'),
        double.parse(finalPosn.longitude ?? '0.0'),
      );

      showDialog(
        context: context,
        builder: (BuildContext context) => const ProgressDialog(
          message: "Please wait",
        ),
      );

      var details = await AssistantMethods.obtainPlaceDirectionDetails(
        pickUpLatLng,
        dropOffLatLng,
      );
      setState(() {
        tripDirectionDetails = details!;
      });

      Navigator.pop(context);

      if (details != null &&
          details.encodedPoints != null &&
          details.encodedPoints!.isNotEmpty) {
        PolylinePoints polylinePoints = PolylinePoints();
        List<PointLatLng> decodePolylinePointsResults =
            polylinePoints.decodePolyline(details.encodedPoints!);

        plineCoordinates.clear();

        if (decodePolylinePointsResults.isNotEmpty) {
          plineCoordinates =
              decodePolylinePointsResults.map((PointLatLng pointLatLng) {
            return LatLng(pointLatLng.latitude, pointLatLng.longitude);
          }).toList();
        }

        setState(() {
          Polyline polyline = Polyline(
            color: Colors.red,
            polylineId: const PolylineId("PolylineID"),
            jointType: JointType.round,
            points: plineCoordinates,
            width: 5,
            startCap: Cap.roundCap,
            endCap: Cap.roundCap,
            geodesic: true,
          );
          polylineSet = {polyline};
        });
        // making the polyline to fit the screen
        LatLngBounds latLngBounds;
        if (pickUpLatLng.latitude > dropOffLatLng.latitude &&
            pickUpLatLng.longitude > dropOffLatLng.longitude) {
          latLngBounds = LatLngBounds(southwest: dropOffLatLng, northeast: pickUpLatLng
          );
        } else if (pickUpLatLng.longitude > dropOffLatLng.longitude) {
          latLngBounds = LatLngBounds(
              southwest: LatLng(pickUpLatLng.latitude, dropOffLatLng.longitude),
              northeast:
                  LatLng(dropOffLatLng.latitude, pickUpLatLng.longitude));
        } else if (pickUpLatLng.latitude > dropOffLatLng.latitude) {
          latLngBounds = LatLngBounds(
              southwest: LatLng(dropOffLatLng.latitude, pickUpLatLng.longitude),
              northeast:
                  LatLng(pickUpLatLng.latitude, dropOffLatLng.longitude));
        } else {
          latLngBounds =
              LatLngBounds(southwest: pickUpLatLng, northeast: dropOffLatLng);
        }
        newGoogleMapController.animateCamera(
          CameraUpdate.newLatLngBounds(latLngBounds, 70),
        );
        // Pick up location Icon
        Marker pickUpLatLngMarker = Marker(
            icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueGreen),
            infoWindow: InfoWindow(
                title: initialPosn.placeName, snippet: "My location"),
            position: pickUpLatLng,
            markerId: const MarkerId("pickUpId"));
        // Drop off location Icon
        Marker dropOffLatLngMarker = Marker(
            icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueYellow),
            infoWindow: InfoWindow(
                title: finalPosn.placeName, snippet: "Drop off location"),
            position: dropOffLatLng,
            markerId: const MarkerId("dropOffId"));
        setState(() {
          markersSet.add(pickUpLatLngMarker);
          markersSet.add(dropOffLatLngMarker);
        });
        Circle pickUpLatLngCircle = Circle(
          fillColor: Colors.redAccent,
          center: pickUpLatLng,
          radius: 12,
          strokeWidth: 4,
          strokeColor: Colors.redAccent,
          circleId: const CircleId("pickUpId"),
        );
        Circle dropOffLatLngCircle = Circle(
          fillColor: Colors.purple,
          center: dropOffLatLng,
          radius: 12,
          strokeWidth: 4,
          strokeColor: Colors.purple,
          circleId: const CircleId("dropOffId"),
        );
        setState(() {
          circlesSet.add(pickUpLatLngCircle);
          circlesSet.add(dropOffLatLngCircle);
        });
      } else {
        print("Error: Direction details are null or invalid");
        print("This is Encoded points:: ");
        if (details != null) {
          print(details
              .encodedPoints); // Access the properties only if details is not null
        } else {
          print(
              'Direction details could not be obtained.'); // Handle the case where details is null
        }
      }
    } else {
      print("Error: Pickup or drop-off location is null");
    }
  }
}

ColorizeAnimatedText buildColorizedText(String text, String fontFamily) {
  return ColorizeAnimatedText(
    text,
    textAlign: TextAlign.center,
    textStyle: TextStyle(
      fontSize: 55.0,
      fontFamily: fontFamily,
    ),
    colors: [
      Colors.green,
      Colors.pink,
      Colors.purple,
      Colors.blue,
      Colors.yellow,
      Colors.red,
    ],
  );
}

