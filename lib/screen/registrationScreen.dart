import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:e_mechanic/main.dart';
import 'package:e_mechanic/screen/loginScreen.dart';

class RegistrationScreen extends StatelessWidget {
  RegistrationScreen({super.key});
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            const SizedBox(height: 45),
            const Image(
              width: 390.0,
              height: 250,
              alignment: Alignment.center,
              image: AssetImage('images/logo.png'),
            ),
            const SizedBox(height: 1),
            const Text(
              'Register as Mechanic',
              style: TextStyle(
                fontSize: 24.0,
                fontFamily: "Brand Bold",
              ),
            ),
            Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: _nameController,
                      keyboardType: TextInputType.name,
                      validator: (val) {
                        if (val!.length < 4 ||
                            val.isEmpty ||
                            !RegExp(r'^[a-z A-Z]+$').hasMatch(val)) {
                          return 'Enter a valid Name';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                        labelText: "Name",
                        labelStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10,
                        ),
                      ),
                      style: const TextStyle(fontSize: 14.0),
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      validator: (val) {
                        if (val!.length < 4 ||
                            val.isEmpty ||
                            !RegExp(
                                r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$')
                                .hasMatch(val)) {
                          return 'Enter a valid Email';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                        labelText: "Email",
                        labelStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10,
                        ),
                      ),
                      style: const TextStyle(fontSize: 14.0),
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      validator: (val) {
                        if (val!.length < 10 ||
                            val.isEmpty ||
                            !RegExp(r'^(?:[+0]9)?[0-9]{10}$').hasMatch(val)) {
                          return 'Enter a valid Phone Number';
                        }
                        return null;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: _phoneController,
                      keyboardType: TextInputType.phone,
                      decoration: const InputDecoration(
                        labelText: "Phone",
                        labelStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10,
                        ),
                      ),
                      style: const TextStyle(fontSize: 14.0),
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      validator: (value) {
                        String missings = '';
                        if (value!.length < 6) {
                          missings += "Password needs at least 6 characters\n";
                        }
                        if (missings.isNotEmpty) {
                          return missings;
                        } else {
                          return null;
                        }
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: _passwordController,
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: const InputDecoration(
                        labelText: "Password",
                        labelStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10,
                        ),
                      ),
                      style: const TextStyle(fontSize: 14.0),
                    ),
                    const SizedBox(height: 25),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize: const Size(370, 50),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        backgroundColor: Colors.yellow.shade700,
                      ),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          registerNewUser();
                          Navigator.of(context).pushNamedAndRemoveUntil('/login', (route) => false).then((value) {
                            showToast();
                          });

                        }
                      },
                      child: const Text(
                        'Create Account',
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Brand Bold",
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Already with Account?"),
                TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                    );
                  },
                  child: const Text('Login Here.'),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  void registerNewUser() async {
    final firebaseUser = (await firebaseAuth.createUserWithEmailAndPassword(
        email: _emailController.text, password: _passwordController.text))
        .user;

    if (firebaseUser != null) {
      // Save user info to realtime database
      Map userDataMap = {
        'name': _nameController.text.trim(),
        'email': _emailController.text.trim(),
        'phone': _phoneController.text.trim()
      };
      userRef.child(firebaseUser.uid).set(userDataMap).then((value) {
      });

    } else {
      Fluttertoast.showToast(
        msg: 'New user Account Has Not Been created',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.yellow.shade700,
        textColor: Colors.white,
      );
    }
  }

  void showToast() {
    Fluttertoast.showToast(
      msg: 'New user Account created',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.yellow.shade700,
      textColor: Colors.white,
    );
  }
}
