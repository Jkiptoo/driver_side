import 'package:e_mechanic/Model/address.dart';
import 'package:e_mechanic/Model/placePredictions.dart';
import 'package:e_mechanic/Widget/Divider.dart';
import 'package:e_mechanic/Widget/progressDialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:e_mechanic/DataHandler/appData.dart';
import 'package:e_mechanic/api/configMaps.dart';

import '../Assistants/requestAssistant.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final _pickUpController = TextEditingController();
  final _destController = TextEditingController();

  List<PlacePrediction> placePredictionsList = [];
  @override
  Widget build(BuildContext context) {
    String placeAddress =
        Provider.of<AppData>(context).pickupLocation?.placeName ?? "";
    _pickUpController.text = placeAddress;
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Container(
              height: 250,
              decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                    color: Colors.black,
                    blurRadius: 6.0,
                    spreadRadius: 0.5,
                    offset: Offset(0.7, 0.7))
              ]),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 25.0, top: 20, right: 25.0, bottom: 20.0),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 30,
                    ),
                    Stack(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(Icons.arrow_back),
                        ),
                        const Center(
                          child: Text(
                            'Set Drop off',
                            style:
                                TextStyle(fontFamily: "Brand-Bold", fontSize: 18),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Image.asset(
                          'images/pickicon.png',
                          height: 16,
                        ),
                        const SizedBox(
                          width: 18,
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade400,
                                borderRadius: BorderRadius.circular(5)),
                            child: Padding(
                              padding: const EdgeInsets.all(3),
                              child: TextField(
                                controller: _pickUpController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Pickup Location',
                                    fillColor: Colors.grey.shade400,
                                    filled: true,
                                    contentPadding: const EdgeInsets.only(
                                        left: 11, top: 8, bottom: 8)),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Image.asset(
                          'images/desticon.png',
                          height: 16,
                        ),
                        const SizedBox(
                          width: 18,
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.grey.shade400,
                                borderRadius: BorderRadius.circular(5)),
                            child: Padding(
                              padding: const EdgeInsets.all(3),
                              child: TextField(
                                onChanged: (val) {
                                  findPlace(val);
                                },
                                controller: _destController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Where to?',
                                    fillColor: Colors.grey.shade400,
                                    isDense: true,
                                    filled: true,
                                    contentPadding: const EdgeInsets.only(
                                        left: 11, top: 8, bottom: 8)),
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            //   tile for listing predictions
            const SizedBox(height: 10.0,),
            (placePredictionsList.isNotEmpty
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 15.0),
                    child: ListView.separated(
                      itemBuilder: (context, index) {
                        return PredictionTile(
                          placePredictions: placePredictionsList[index],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const DividerWidget(),
                      itemCount: placePredictionsList.length,
                      shrinkWrap: true,
                      physics: const ClampingScrollPhysics(),
                    ),
                  )
                : Container())
          ],
        ),
      ),
    );
  }

  void findPlace(String placeName) async {
    if (placeName.length > 1) {
      String autoCompleteUrl =
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$placeName&types=geocode&key=$mapKey";
      var res = await RequestAssistant.getRequest(autoCompleteUrl);

      if (res == "failed") {
        return;
      }
      if (res["status"] == "OK") {
        var predictions = res["predictions"];

        var placeList = (predictions as List)
            .map((e) => PlacePrediction.fromJson(e))
            .toList();
        setState(() {
          placePredictionsList = placeList;
        });
      }
    }
  }
}

class PredictionTile extends StatelessWidget {
  final PlacePrediction? placePredictions;

  const PredictionTile({Key? key, this.placePredictions}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        if (placePredictions != null) {
          getPlaceAddressDetails(placePredictions!.place_id, context);
        }
      },
      child: SizedBox(
        child: Column(
          children: [
            const SizedBox(
              width: 10,
            ),
            Row(
              children: [
                const Icon(Icons.add_location),
                const SizedBox(width: 14),
                Expanded(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 8),
                        Text(
                          '${placePredictions?.main_text ?? ""}',
                          style: const TextStyle(
                              fontSize: 16, overflow: TextOverflow.ellipsis),
                        ),
                        const SizedBox(height: 2),
                        Text(
                          '${placePredictions?.secondary_text ?? ""}',
                          style: const TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              overflow: TextOverflow.ellipsis),
                        ),
                        const SizedBox(height: 10),
                      ]),
                )
              ],
            ),
            const SizedBox(
               width: 10,
            ),
          ],
        ),
      ),
    );
  }
  void getPlaceAddressDetails(String? placeId, context) async {
    if (placeId == null) {
      return;
    }
    showDialog(context: context, builder: (BuildContext context) {
     return const ProgressDialog(message: "Setting Drop off please wait");
    });
    String placeAddressUrl =
        "https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&key=$mapKey";
    var res = await RequestAssistant.getRequest(placeAddressUrl);
    Navigator.of(context).pop();
    if (res == null) {
      return;
    }
    if (res["status"] == "OK") {
      Address address = Address();
      address.placeName = res["result"]["name"];
      address.placeId = placeId;
      address.latitude = res["result"]["geometry"]["location"]["lat"].toString();
      address.longitude = res["result"]["geometry"]["location"]["lng"].toString();

      Provider.of<AppData>(context, listen: false)
          .updateDropOffLocationAddress(address);
      print("This is DropOff Location::");
      print(address.placeName);
      Navigator.pop(context, "obtainDirection");
    }
  }
}
