import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:e_mechanic/screen/registrationScreen.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  static const idScreen = 'login';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            const SizedBox(
              height: 45,
            ),
            const Image(
                width: 390.0,
                height: 250,
                alignment: Alignment.center,
                image: AssetImage('images/logo.png')),
            const SizedBox(height: 1),
            const Text(
              'Login as Driver',
              style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
            ),
            Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: [
                      TextFormField(
                        validator: (val) {
                          if (val!.length < 4 ||
                              val.isEmpty ||
                              !RegExp(r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$')
                                  .hasMatch(val)) {
                            return 'Enter a valid Email';
                          }
                          return null;
                        },
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: const InputDecoration(
                          labelText: "Email",
                          labelStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 10,
                          ),
                        ),
                        style: const TextStyle(fontSize: 14.0),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        validator: (value) {
                          String missings = '';
                          if (value!.length < 6) {
                            missings +=
                            "Password needs at least 6 characters\n";
                          }
                          //if there is password input errors return error string
                          if (missings.isNotEmpty) {
                            return missings;
                          } else {
                            return null;
                          }
                        },
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        controller: _passwordController,
                        obscureText: true,
                        keyboardType: TextInputType.visiblePassword,
                        decoration: const InputDecoration(
                          labelText: "Password",
                          labelStyle:
                          TextStyle(color: Colors.grey, fontSize: 10),
                        ),
                        style: const TextStyle(fontSize: 14.0),
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              minimumSize: const Size(370, 50),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              backgroundColor: Colors.yellow.shade700),
                          onPressed: () async{
                            if (_formKey.currentState!.validate())  {
                              await FirebaseAuth.instance
                                  .signInWithEmailAndPassword(
                                  email: _emailController.text,
                                  password: _passwordController.text)  .catchError((err) {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: const Text("Error"),
                                      content: Text(err.toString()),
                                      actions: [
                                        ElevatedButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text("Ok"),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              })
                                  .then((value) {
                                Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
                                Fluttertoast.showToast(
                                  msg: 'User Logged In Successfully',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.yellow.shade700,
                                  textColor: Colors.white,
                                );
                              });
                            }
                          },
                          child: const Text(
                            'Login',
                            style: TextStyle(
                                fontSize: 18, fontFamily: "Brand Bold"),
                          ))
                    ],
                  ),
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Don't have an Account?"),
                TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegistrationScreen()));
                    },
                    child: const Text('Register Here.'))
              ],
            )
          ],
        ),
      ),
    );
  }
}
