import 'package:flutter/material.dart';

class ProgressDialog extends StatelessWidget {
  const ProgressDialog({super.key, required this.message});
  final String message;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.yellow,
      child: Container(
          margin: const EdgeInsets.all(15.0),
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6.0)
          ),
          child: const Padding(
            padding: EdgeInsets.all(15),
            child:
            Row(
              children: [
                SizedBox(
                  width: 6.0,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                ),
                SizedBox(
                  width: 25,
                )
              ],
            ),
          )
      ),
    );
  }
}
